package org.java8.hello;

public class RunnableExample {

	public static void main(String[] args) {
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.println("Print runnable run method");
			}
		});
		t.run();
		
		Thread lt = new Thread(() -> System.out.println("Print runnable run Lambda method"));
		lt.run();
	}
}
