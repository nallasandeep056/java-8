package org.java8.hello;

public class Greeter {

	public void greeting(Greeting greeting) {
		greeting.greet();
	}
	
	public static void main(String[] args) {
		Greeter greeter = new Greeter();
		
		Greeting greeting = () -> System.out.println("Greeting");
		greeter.greeting(greeting);
		
		Greeting greeting1 = new Greeting() {
			public void greet() {
				System.out.println("Greeting 1");
			}
		};
		greeter.greeting(greeting1);
	}
}
