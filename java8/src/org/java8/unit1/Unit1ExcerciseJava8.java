package org.java8.unit1;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

public class Unit1ExcerciseJava8 {

	public static void main(String[] args) {
		List<Person> persons = Arrays.asList(
				new Person("Srikanth", "Vallepu", 28),
				new Person("Sandeep", "Nalla", 26),
				new Person("Vidyanath", "Nalla", 28),
				new Person("Rajkumar", "Sabavath", 26));
		
		// 0. Before Sorting
		System.out.println("Before Sorting: \n");
		printConditionally(persons, (p) -> true);
		
		// 1. Sort the List by Last name
		
		persons.sort((p1, p2) -> p1.getLastName().compareTo(p2.getLastName()));
		
		// 2. Print the List
		System.out.println("\n\nAfter Sorting with last name: \n");
		printConditionally(persons, (p) -> true);

		// 3. Print the list having last name starting with N
		System.out.println("\n\nStarting with 'N':\n");
		printConditionally(persons, p -> p.getLastName().startsWith("N"));

		// 4. Print the list having first name starting with S
		System.out.println("\n\nStarting with 'S':\n");
		printConditionally(persons, p -> p.getFirstName().startsWith("S"));

		// 5. Print the list having age 26
		System.out.println("\n\nWith age 26:\n");
		printConditionally(persons, p -> p.getAge().equals(26));
	}

	private static void printConditionally(List<Person> persons, Predicate<Person> condition) {
		for (Person p : persons) {
			if (condition.test(p)) {
				System.out.println(p);
			}
		}
	}

}
