package org.java8.hello;

@FunctionalInterface
public interface Greeting {

	public void greet();
}
