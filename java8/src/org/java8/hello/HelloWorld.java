package org.java8.hello;

public class HelloWorld implements Greeting {

	@Override
	public void greet() {
		System.out.println("Hello world!");
	}
}
