package org.java8.unit2;

public class ThisReferenceLambdaExample {
	
	private void doProcess(int i, Process process) {
		process.process(i);
	}
	
	private void execute(int j) {
		doProcess(j, i -> {
			System.out.println("The value of i is "+i);
			System.out.println(this);
		});
	}

	public static void main(String[] args) {
		ThisReferenceLambdaExample thisRef = new ThisReferenceLambdaExample();
		thisRef.doProcess(10, i -> {
				System.out.println("The value of i is "+i);
				//System.out.println(this);
			});
		thisRef.execute(10);
	}

	@Override
	public String toString() {
		return "ThisReferenceLambdaExample [getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}
	
	

	
}
