package org.java8.unit3;

import java.util.Arrays;
import java.util.List;

import org.java8.unit1.Person;

public class StreamsExample {

	public static void main(String[] args) {
		List<Person> persons = Arrays.asList(
				new Person("Srikanth", "Vallepu", 28),
				new Person("Sandeep", "Nalla", 26),
				new Person("Vidyanath", "Nalla", 28),
				new Person("Rajkumar", "Sabavath", 26));
		
		persons.stream()
		.filter(p->p.getLastName().startsWith("N"))
		.forEach(p->System.out.println(p.getFirstName()));
		long count = persons.stream()
		.filter(p->p.getLastName().startsWith("N"))
		.count();
		System.out.println(count);
		persons.parallelStream()
		.filter(p->p.getLastName().startsWith("N"))
		.forEach(p->System.out.println(p.getFirstName()));
	}

}
