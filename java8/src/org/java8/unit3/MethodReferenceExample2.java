package org.java8.unit3;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

import org.java8.unit1.Person;

public class MethodReferenceExample2 {

	public static void main(String[] args) {
		List<Person> persons = Arrays.asList(
				new Person("Srikanth", "Vallepu", 28),
				new Person("Sandeep", "Nalla", 26),
				new Person("Vidyanath", "Nalla", 28),
				new Person("Rajkumar", "Sabavath", 26));
		
		performConditionally(persons, (p) -> true, p -> System.out.println(p));
		System.out.println("Using Method Reference:");
		performConditionally(persons, (p) -> true, System.out::println);
	}

	private static void performConditionally(List<Person> persons, Predicate<Person> condition, Consumer<Person> consumer) {
		for (Person p : persons) {
			if (condition.test(p)) {
				consumer.accept(p);
			}
		}
	}

}
