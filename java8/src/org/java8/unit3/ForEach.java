package org.java8.unit3;

import java.util.Arrays;
import java.util.List;

import org.java8.unit1.Person;

public class ForEach {

	public static void main(String[] args) {
		List<Person> persons = Arrays.asList(
				new Person("Srikanth", "Vallepu", 28),
				new Person("Sandeep", "Nalla", 26),
				new Person("Vidyanath", "Nalla", 28),
				new Person("Rajkumar", "Sabavath", 26),
				new Person("Srikanth", "Vallepu", 28),
				new Person("Sandeep", "Nalla", 26),
				new Person("Vidyanath", "Nalla", 28),
				new Person("Rajkumar", "Sabavath", 26),
				new Person("Srikanth", "Vallepu", 28),
				new Person("Sandeep", "Nalla", 26),
				new Person("Vidyanath", "Nalla", 28),
				new Person("Rajkumar", "Sabavath", 26));
		
		/**
		 * For loop
		 */
		System.out.println("Using For loop:");
		for(int i=0; i < persons.size(); i++) {
			System.out.println(persons.get(i));
		}
		
		/**
		 * For in loop
		 */
		System.out.println("Using For In loop:");
		for(Person p: persons) {
			System.out.println(p);
		}

		/**
		 * Using for each
		 */
		
		System.out.println("Using for each:");
		persons.forEach(p->System.out.println(p));
		System.out.println("Method reference:");
		persons.forEach(System.out::println);
	}

}
