package org.java8.unit2;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

import org.java8.unit1.Person;

public class StandardFunctionalInterfaces {

	public static void main(String[] args) {
		List<Person> persons = Arrays.asList(
				new Person("Srikanth", "Vallepu", 28),
				new Person("Sandeep", "Nalla", 26),
				new Person("Vidyanath", "Nalla", 28),
				new Person("Rajkumar", "Sabavath", 26));
		
		// 0. Before Sorting
		System.out.println("Before Sorting: \n");
		performConditionally(persons, (p) -> true, p -> System.out.println(p));
		
		// 1. Sort the List by Last name
		
		persons.sort((p1, p2) -> p1.getLastName().compareTo(p2.getLastName()));
		
		// 2. Print the List
		System.out.println("\n\nAfter Sorting with last name: \n");
		performConditionally(persons, (p) -> true,  p -> System.out.println(p));

		// 3. Print the list having last name starting with N
		System.out.println("\n\nStarting with 'N':\n");
		performConditionally(persons, p -> p.getLastName().startsWith("N"),  p -> System.out.println(p));

		// 4. Print the list having first name starting with S
		System.out.println("\n\nStarting with 'S':\n");
		performConditionally(persons, p -> p.getFirstName().startsWith("S"),  p -> System.out.println(p));

		// 5. Print the list having age 26
		System.out.println("\n\nFirst name of the person with age 26:\n");
		performConditionally(persons, p -> p.getAge().equals(26),  p -> System.out.println(p.getFirstName()));
	}

	private static void performConditionally(List<Person> persons, Predicate<Person> condition, Consumer<Person> consumer) {
		for (Person p : persons) {
			if (condition.test(p)) {
				consumer.accept(p);
			}
		}
	}

}
