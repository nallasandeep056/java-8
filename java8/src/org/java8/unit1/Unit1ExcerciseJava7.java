package org.java8.unit1;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Unit1ExcerciseJava7 {

	public static void main(String[] args) {
		List<Person> persons = Arrays.asList(
				new Person("Srikanth", "Vallepu", 28),
				new Person("Sandeep", "Nalla", 26),
				new Person("Vidyanath", "Nalla", 28),
				new Person("Rajkumar", "Sabavath", 26));
		
		// 0. Before Sorting
		System.out.println("Before Sorting: \n");
		printAll(persons);
		
		// 1. Sort the List by Last name
		
		persons.sort(new Comparator<Person>() {
			@Override
			public int compare(Person o1, Person o2) {
				return o1.getLastName().compareTo(o2.getLastName());
			}
		});
		
		// 2. Print the List
		System.out.println("\n\nAfter Sorting: \n");
		printAll(persons);

		// 3. Print the list having last name starting with N
		System.out.println("\n\nStarting with 'N':\n");
		printConditionally(persons, new Condition() {
			
			@Override
			public boolean test(Person person) {
				return person.getLastName().startsWith("N");
			}
		});

		// 4. Print the list having first name starting with S
		System.out.println("\n\nStarting with 'S':\n");
		printConditionally(persons, new Condition() {
			@Override
			public boolean test(Person person) {
				return person.getFirstName().startsWith("S");
			}
		});

		// 5. Print the list having age 26
		System.out.println("\n\nWith age 26:\n");
		printConditionally(persons, new Condition() {
			@Override
			public boolean test(Person person) {
				return person.getAge().equals(26);
			}
		});
	}

	private static void printConditionally(List<Person> persons, Condition condition) {
		for (Person p : persons) {
			if (condition.test(p)) {
				System.out.println(p.toString());
			}
		}
	}

	private static void printAll(List<Person> persons) {
		for (Person p : persons) {
			System.out.println(p);
		}
	}

}
