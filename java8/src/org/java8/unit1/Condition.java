package org.java8.unit1;

public interface Condition {
	
	boolean test(Person person);

}
