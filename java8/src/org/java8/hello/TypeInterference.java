package org.java8.hello;

public class TypeInterference {

	public static void main(String[] args) {
		printLambda(s -> s.length());
	}

	private static void printLambda(StringLength length) {
		System.out.println(length.getLength("Hello Rajkumar"));
	}

	interface StringLength {
		int getLength(String s);
	}
}
