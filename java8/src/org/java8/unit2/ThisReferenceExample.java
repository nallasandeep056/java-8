package org.java8.unit2;

public class ThisReferenceExample {
	
	private void doProcess(int i, Process process) {
		process.process(i);
	}

	public static void main(String[] args) {
		ThisReferenceExample thisRef = new ThisReferenceExample();
		thisRef.doProcess(10, new Process() {
			@Override
			public void process(int i) {
				System.out.println("The value of i is "+i);
				System.out.println(this);
			}
			@Override
			public String toString() {
				return "This is inner class Example";
			}
		});
	}

	
}
